import { Component, OnInit } from '@angular/core';
import { WordService } from '../../services/word.service';

@Component({
  selector: 'app-word',
  templateUrl: './word.component.html',
  styleUrls: ['./word.component.css']
})
export class WordComponent implements OnInit {

  firstWord: string;
  secondWord: string;
  relationship: string;

  constructor(private service: WordService) { }

  ngOnInit() {
  }

  getRelationship() {
    this.service.isSynonym(this.firstWord, this.secondWord)
      .subscribe(tf => {
        if (tf === true) {
          this.relationship = "Synonyms";
        }
        else {
          this.service.isAntonym(this.firstWord, this.secondWord)
            .subscribe(tf => {
              if (tf === true) {
                this.relationship = "Antonym";
              }
              else {
                this.service.isRelated(this.firstWord, this.secondWord)
                  .subscribe(tf => {
                    this.relationship = tf === true ? "Related" : "Unknown"
                  });
              }
            });
        }
      });
  }

}
