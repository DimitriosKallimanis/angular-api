import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';

const baseUrl = 'https://api.datamuse.com/words/';

@Injectable({
  providedIn: 'root'
})
export class WordService {

  constructor(private http: HttpClient) { }

  isSynonym(firstWord: string, secondWord: string): Observable<boolean> {
    return this.http.get(baseUrl, {
      params: {
        rel_syn: firstWord
      }
    })
    .map(data=>data as Array<any>)
    .map(data=>data.map(d=>d.word))
    .map(data=>data.indexOf(secondWord)>-1);
  }

  isAntonym(firstWord: string, secondWord: string): Observable<boolean> {
    return this.http.get(baseUrl, {
      params: {
        rel_ant: firstWord
      }
    })
    .map(data=>data as Array<any>)
    .map(data=>data.map(d=>d.word))
    .map(data=>data.indexOf(secondWord)>-1);
  }

  isRelated(firstWord: string, secondWord: string): Observable<boolean> {
    return this.http.get(baseUrl, {
      params: {
        rel_spc: firstWord
      }
    })
    .map(data=>data as Array<any>)
    .map(data=>data.map(d=>d.word))
    .map(data=>data.indexOf(secondWord)>-1);
  }
}
